<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>TypesCuisinesDAO : tests unitaires</title>
    </head>

    <body>

        <?php

        use modele\dao\TypeCuisineDAO;
        use modele\metier\TypeCuisine;
        use modele\dao\UtilisateurDAO;
        use modele\dao\Bdd;

require_once '../../includes/autoload.inc.php';

        try {
            Bdd::connecter();
            ?>
            <h2>Test TypeCuisinesDAO</h2>

            
            
            <h3>3- getAllByResto</h3>
            <?php $idR = 8; ?>
            <p>Les critiques pour le restaurant n° <?= $idR ?></p>
            <?php
            $desTypes = TypeCuisineDAO::getAllType($idR);
            var_dump($desTypes);
            ?>
          
            <?php
            

            Bdd::deconnecter();
        } catch (Exception $ex) {
            ?>
            <h4>*** Erreur récupérée : <br/> <?= $ex->getMessage() ?> <br/>***</h4>
            <?php
        }
        ?>

    </body>
</html>
