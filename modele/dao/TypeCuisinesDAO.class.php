<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace modele\dao;

use modele\dao\TypeCuisinesDAO;
use modele\metier\Resto;
use modele\dao\Bdd;
use modele\metier\TypeCuisine;
use PDO;
use PDOException;
use Exception;
/**
 * Description of TypeCusinesDAO
 *
 * @author aikaf
 */
class TypeCuisinesDAO {
    
    public static function getAllType(): array {
        $lesObjets = array();
        try {            
            $requete = "SELECT * FROM type_cuisines";
            $stmt = Bdd::getConnexion()->prepare($requete);
            $ok = $stmt->execute();
            // attention, $ok = true pour un select ne retournant aucune ligne
            if ($ok) {
                // Pour chaque enregistrement
                while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    //Instancier un nouveau typeCuisine et l'ajouter à la liste
                    $lesObjets[] = self::enregistrementVersObjet($enreg);
                }
            }
        } catch (PDOException $e) {
            throw new Exception("Erreur dans la méthode " . get_called_class() . "::getAllType : <br/>" . $e->getMessage());
        }
        return $lesObjets;
    }
    public static function getAllSaufUtilisateur(int $idU): array{
        $lesObjets = array();
        try {
            $requete = "SELECT tc.* FROM type_cuisines tc
                        WHERE tc.idTC NOT IN (SELECT idTC FROM prefere WHERE idU =:idU)";
            $stmt = Bdd::getConnexion()->prepare($requete);
            $stmt->bindParam(':idU', $idU, PDO::PARAM_INT);
            $ok = $stmt->execute();
            if ($ok) {
                while($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $lesObjets[]= self::enregistrementVersObjet($enreg);
                }
            }
        } catch (Exception $e) {
            throw new Exception("Erreur dans la méthode " . get_called_class() . "::getAllType : <br/>" . $e->getMessage());
        }
        return $lesObjets;
    }
    public static function getPrefererByIdU(int $idU): array {
        $lesObjets = array();
        try {
            $requete = "SELECT type_cuisines.* from type_cuisines
                        INNER JOIN prefere ON type_cuisines.idTC = prefere.idTC
                        WHERE idU = :idU";
            $stmt = Bdd::getConnexion()->prepare($requete);
            $stmt->bindParam(':idU', $idU, PDO::PARAM_INT);
            $ok = $stmt->execute();
            // attention, $ok = true pour un select ne retournant aucune ligne
            if ($ok) {
                // Pour chaque enregistrement
                while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    //Instancier un nouveau restaurant et l'ajouter à la liste
                    $lesObjets[] = new TypeCuisine
                            (
                            $enreg['idTC'], $enreg['libelleTC']
                    );
                }
            }
        } catch (PDOException $e) {
            throw new Exception("Erreur dans la méthode " . get_called_class() . "::getAimesByIdU : <br/>" . $e->getMessage());
        }
        return $lesObjets;
    }
    private static function enregistrementVersObjet(array $enreg): TypeCuisine {
        // Instanciation sans les associations
        $leTypeCuisine = new TypeCuisine(
                $enreg['idTC'], $enreg['libelleTC']
        );
    
        return $leTypeCuisine;
    }
    public static function getAllTypeByUnResto(int $id): array { 
        $lesTypes = array();
        try {            
            $requete = "SELECT *
                        FROM type_cuisines tc
                        INNER JOIN resto_types rt ON tc.idTC = rt.idTC
                        AND rt.idR = ".$id.";";
            $stmt = Bdd::getConnexion()->prepare($requete);
            $ok = $stmt->execute();
            // attention, $ok = true pour un select ne retournant aucune ligne
            if ($ok) {
                // Pour chaque enregistrement
                while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    //Instancier un nouveau restaurant et l'ajouter à la liste
                    //$lesTypes[] = $enreg;
                    $lesTypes[] = new TypeCuisine
                    ($enreg['idTC'], $enreg['libelleTC']);

                }
            }
        } catch (PDOException $e) {
            throw new Exception("Erreur dans la méthode " . get_called_class() . "::getAllType : <br/>" . $e->getMessage());
        }
        return $lesTypes;
    }


}

