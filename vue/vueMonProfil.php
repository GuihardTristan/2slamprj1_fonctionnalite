<?php
/**
 * --------------
 * vueMonProfil
 * --------------
 * 
 * @version 07/2021 par NB : intégration couche modèle objet
 * 
 * Variables transmises par le contrôleur detailResto contenant les données à afficher : 
  ---------------------------------------------------------------------------------------- */
/** @var Utilisateur  $util utilisteur à afficher */

/** @var array $mesRestosAimes  */
/** @var int $idU  */
/** @var string $mailU  */
/**
 * Variables supplémentaires :  
  ------------------------- */
/** @var Resto $unResto */

?>

<h1>Mon profil</h1>

Mon adresse électronique : <?= $util->getMailU() ?> <br />
Mon pseudo : <?= $util->getPseudoU() ?> <br />

<hr>

les restaurants que j'aime : <br />
<?php
foreach ($mesRestosAimes as $unResto) {
    ?>
    <a href="./?action=detail&idR=<?= $unResto->getIdR() ?>"><?= $unResto->getNomR() ?></a><br />
    <?php
}
?>
<hr>

les types de restaurants que j'aime :
<?php
foreach ($mesTypesPreferer as $unType) {
    ?>
<div class ="cadre"><span style="color: red;">#</span><?= $unType->getLibelleTC() ?></div>
    <?php
}
?>

<hr>
<a href="./?action=deconnexion">se deconnecter</a>
<style>
        .cadre {
            border: 2px solid #000; /* Couleur de la bordure et épaisseur */
            padding: 10px; /* Espace intérieur du cadre */
            border-radius: 5px; /* Coins arrondis (facultatif) */
        }
</style>


