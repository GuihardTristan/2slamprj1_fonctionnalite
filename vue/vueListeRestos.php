<?php
foreach ($listeRestos as $unResto) {
    
    $lesPhotos = $unResto->getLesPhotos();
    $lesTypes = $unResto->getLesTypes(); // Récupérez les types de cuisine

    ?>
    <div class="card">
        <div class="photoCard">
            <?php
            if (count($lesPhotos) > 0) {
                $unePhoto = $lesPhotos[0];
                ?>
                <img src="photos/<?= $unePhoto->getCheminP() ?>" alt="photo du restaurant" />
                <?php
            }
            ?>

        </div>
        <div class="descrCard">
            <a href="./?action=detail&idR=<?= $unResto->getIdR() ?>"><?= $unResto->getNomR() ?></a>
            <br />
            <?= $unResto->getNumAdr() ?>
            <?= $unResto->getVoieAdr() ?>
            <br />
            <?= $unResto->getCpR() ?>
            <?= $unResto->getVilleR() ?>

            <br />
            <br />
            

            <?php
            // Créez un tableau vide pour stocker les libellés des types de cuisine
            $typesArray = array();

            // Itérez sur les types de cuisine et ajoutez les libellés au tableau
            foreach ($lesTypes as $type) {
               $typesArray[] = '#' . $type->getLibelleTC();
            }

            // Transformez le tableau en une chaîne de caractères en utilisant la fonction implode
            $typesString = implode(' #', $typesArray);

            echo $typesString;
            ?>

        </div>
    </div>
    <?php
}
?>
