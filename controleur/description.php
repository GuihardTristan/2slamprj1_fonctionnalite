<?php

use modele\dao\Bdd;
use modele\dao\CritiqueDAO;
use modele\dao\UtilisateurDAO;
use modele\metier\Critique;
use modele\dao\RestoDAO;

/**
 * Contrôleur commenter
 * Ajout/mise à jour du commentaire critique portée par l'utilisateur courant sur un restaurant
 * 
 * Vue contrôlée : aucune
 * Données GET : 
 *      - $idR identifiant du restaurant concerné
 *      - $commentaire commentaire apporté par l'utilisateur courant sur le restaurant
 * 
 * @version 07/2021 intégration couche modèle objet
 * @version 08/2021 gestion erreurs
 */
Bdd::connecter();


// Récupération des données GET, POST, et SESSION
    $idR = intval($_GET["idR"]);
    $description = $_POST["description"];
// Un utilisateur doit être connecté
    $idU = getIdULoggedOn();
    if ($idU != 0) {
            // simplement mettre à jour le commentaire de la critique
            RestoDAO::updateDescription($idR, $description);
        }
// redirection vers la page appelante
    header('Location: ' . $_SERVER['HTTP_REFERER']);
?>